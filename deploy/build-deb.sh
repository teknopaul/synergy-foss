#!/bin/bash -e
#
# Build a binary .deb package
#
test $(id -u) == "0" || (echo "Run as root" && exit 1) # requires bash -e

#
# The package name
#
name=synergy-foss
arch=$(uname -m)

cd $(dirname $0)/..
project_root=$PWD

#
# Create a temporary build directory
#
tmp_dir=/tmp/$name-debbuild
rm -rf $tmp_dir
mkdir -p $tmp_dir/DEBIAN $tmp_dir/usr/bin/ $tmp_dir/etc

. ./version
sed -e "s/@PACKAGE_VERSION@/$VERSION/" $project_root/deploy/DEBIAN/control.in > $tmp_dir/DEBIAN/control

strip bin/synergy*
cp bin/synergy* $tmp_dir/usr/bin/
cp doc/synergy.conf.example $tmp_dir/etc/

size=$(du -sk $tmp_dir | cut -f 1)
sed -i -e "s/@SIZE@/$size/" $tmp_dir/DEBIAN/control

#
# setup conffiles
#
(
  cd $tmp_dir/
  find etc -type f | sed 's.^./.' > DEBIAN/conffiles
)

#
# Setup the installation package ownership here if it needs root
#
chown -R root.root $tmp_dir

#
# Build the .deb
#
mkdir -p target/
dpkg-deb --build $tmp_dir target/$name-$VERSION-1.$arch.deb

test -f target/$name-$VERSION-1.$arch.deb

echo "built target/$name-$VERSION-1.$arch.deb"

if [ -n "$SUDO_USER" ]
then
  chown $SUDO_USER target/ target/$name-$VERSION-1.$arch.deb
fi

test -d $tmp_dir && rm -rf $tmp_dir
